from src.entities import Bet, EventStatus, Event
from src.repositories import BetRepository


class BetService:
    """Сервис для бизнеслогики ставок"""

    def __init__(self, bet_repository: BetRepository):
        self._repo = bet_repository

    async def create_bet(self, bet: Bet) -> Bet:
        """Совершает ставку на событие"""
        await self._repo.create_bet(bet)
        return bet

    async def get_bets(self, event_id: str) -> list[Bet]:
        """Возвращает историю всех сделанных ставок на евент"""
        bets = await self._repo.get_all(event_id=event_id)
        return bets

    async def change_event(self, event_id: str, new_status: EventStatus) -> Event:
        """Сообщает о том, что в событии с указанным event_id произошли изменения"""
        bets = await self._repo.get_all(event_id=event_id)
        for bet in bets:
            if bet.first_contender_win and new_status == EventStatus.WIN:
                await self._set_win_bet(bet)
            elif not bet.first_contender_win and new_status == EventStatus.LOSE:
                await self._set_win_bet(bet)
            else:
                await self._set_lose_bet(bet)

        return Event(
            event_id=event_id,
            status=new_status,
        )

    async def _set_lose_bet(self, bet: Bet) -> Bet:
        """Меняет статус ставки на проигрыш"""
        bet.bet_win = False
        return await self._repo.save(bet)

    async def _set_win_bet(self, bet: Bet) -> Bet:
        """Меняет статус ставки на выйгрыш"""
        bet.bet_win = True
        return await self._repo.save(bet)
