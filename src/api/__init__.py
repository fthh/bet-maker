from fastapi import FastAPI

from .handlers.health_check import router as health_router
from .handlers.events import router as events_router
from .handlers.bets import router as bets_router


def init_routes(app: FastAPI) -> FastAPI:
    """Добавляет все роутеры в FastAPI app"""
    app.include_router(health_router)
    app.include_router(events_router)
    app.include_router(bets_router)
    return app
