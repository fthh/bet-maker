from fastapi import APIRouter, Depends

from dependency_injector.wiring import inject, Provide

from src.api.deps import IocContainer
from src.api.schema import CreateBetSchemaRequest
from src.entities import Bet
from src.logic.services import BetService

router = APIRouter()


@router.post('/bets', tags=['bets'])
@inject
async def create_bet(
    data: CreateBetSchemaRequest,
    bet_service: BetService = Depends(Provide[IocContainer.bet_service]),
) -> Bet:
    """HTTP обработчик для создания ставки на событие"""
    bet = await bet_service.create_bet(Bet(
        bet_id=data.bet_id,
        event_id=data.event_id,
        first_contender_win=data.first_contender_win,
    ))
    return bet


@router.get('/bets', tags=['bets'])
@inject
async def get_bets(
    event_id: str,
    bet_service: BetService = Depends(Provide[IocContainer.bet_service]),
) -> list[Bet]:
    """HTTP обработчик получения ставок"""
    return await bet_service.get_bets(event_id)
