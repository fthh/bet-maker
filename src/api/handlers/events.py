from fastapi import APIRouter, Depends
from dependency_injector.wiring import inject, Provide

from src.api.deps import IocContainer
from src.api.schema.events import ChangeEventSchemaRequest
from src.entities import Event
from src.logic.services import BetService

router = APIRouter()


@router.put('/events', tags=['events'])
@inject
async def change_event(
    data: ChangeEventSchemaRequest,
    bet_service: BetService = Depends(Provide[IocContainer.bet_service]),
) -> Event:
    """HTTP обработчик для измнения события на которое ставились ставки"""
    event = await bet_service.change_event(data.event_id, data.new_status)
    return event
