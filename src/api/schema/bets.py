from pydantic import BaseModel, Field

from src.entities import generate_id


class CreateBetSchemaRequest(BaseModel):
    """Схема апи для создания ставки"""

    bet_id: str = Field(default_factory=generate_id)
    event_id: str
    amount: int
    first_contender_win: bool

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "bet_id": "6f939d70-cd48-4f7c-b411-42f816d0e248",
                    "event_id": "199fd8fe-1986-45f3-b95f-53ac67d656a0",
                    "amount": 100,
                    "first_contender_win": True,
                }
            ]
        }
    }
