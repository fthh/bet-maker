from pydantic import BaseModel

from src.entities import EventStatus


class ChangeEventSchemaRequest(BaseModel):
    event_id: str
    new_status: EventStatus

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "new_status": "WIN",
                    "event_id": "199fd8fe-1986-45f3-b95f-53ac67d656a0",
                },
                {
                    "new_status": "LOSE",
                    "event_id": "199fd8fe-1986-45f3-b95f-53ac67d656a0",
                }
            ]
        }
    }
