from fastapi import FastAPI

from src.api import init_routes
from src.api.deps import IocContainer


async def create_app(app) -> FastAPI:
    app = init_routes(app)

    container = IocContainer()
    bet_repo = container.bet_repo()
    await bet_repo.init()

    container.wire(packages=['src.api'])
    app.container = container
    return app
