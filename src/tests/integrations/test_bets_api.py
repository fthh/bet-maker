import pytest

from src.repositories import BetRepository


@pytest.mark.anyio
async def test_get_bets_empty(api_client):
    """Получение списка ставок (хранилище ставок пустое)"""
    response = api_client.get("/bets", params={'event_id': '8236da5c-23c2-493c-aed7-5d0c76f47c65'})

    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.asyncio
async def test_get_bets_empty_by_wrong_event_id(api_client, bet):
    """Получение пустого списка ставок (ставок с таким event_id нет в системе)"""
    bet_repo = BetRepository()
    await bet_repo.init()

    with api_client.app.container.bet_repo.override(bet_repo):
        await bet_repo.delete_all()
        await bet_repo.create_bet(bet)

        response = api_client.get("/bets", params={'event_id': '5f7237b5-08c0-4342-b3e0-be9efe86bec4'})

    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.asyncio
async def test_get_bets(api_client, bet):
    """Получение списка ставок"""
    bet_repo = BetRepository()
    await bet_repo.init()
    with api_client.app.container.bet_repo.override(bet_repo):
        await bet_repo.delete_all()
        await bet_repo.create_bet(bet)

        response = api_client.get("/bets", params={'event_id': bet.event_id})

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json() == [
        {
            'bet_id': bet.bet_id,
            'event_id': bet.event_id,
            'first_contender_win': True,
            'bet_win': None,
        }
    ]
