import pytest

from src.entities import EventStatus, generate_id
from src.repositories import BetRepository


@pytest.mark.anyio
async def test_change_events(api_client, bet):
    """Измнение события"""
    bet_repo = BetRepository()
    await bet_repo.init()

    bet.event_id = str(generate_id())
    with api_client.app.container.bet_repo.override(bet_repo):
        await bet_repo.delete_all()
        await bet_repo.create_bet(bet)

        response = api_client.put(
            "/events",
            json={
                'event_id': bet.event_id,
                'new_status': EventStatus.WIN.value,
            }
        )

    assert response.status_code == 200
    assert response.json() == [
        {
            'bet_id': bet.event_id,
            'event_id': bet.event_id,
            'first_contender_win': True,
            'bet_win': True,
        }
    ]

