import pytest

from src.entities import Bet
from src.repositories import BetRepository


@pytest.fixture
def bet_repo():
    return BetRepository()


@pytest.fixture
def bet():
    return Bet(
        bet_id='65569de5-4922-452c-aae3-87631eba8b71',
        event_id='c134281f-bd13-473d-b727-bcc2b5e6b230',
        first_contender_win=True,
    )

