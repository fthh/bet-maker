import asyncpg

from src.entities import Bet
from src.settings import environment


class BetRepository:
    def __init__(self):
        self._data = []
        self._pool = None

    async def init(self):
        self._pool = await asyncpg.create_pool(
            dsn=environment.DB_DSN,
        )
        await self._exec_sql("""
            CREATE TABLE IF NOT EXISTS bets (
                bet_id uuid NOT NULL,
                event_id uuid NOT NULL,
                first_contender_win bool NOT NULL,
                bet_win bool NULL
            );
        """)

    async def create_bet(self, bet: Bet) -> Bet:
        """Создаёт ставку в БД"""
        await self._exec_sql("""
            INSERT INTO bets
            ( bet_id, event_id, first_contender_win, bet_win )
            VALUES
            ($1, $2, $3, $4);
        """, bet.bet_id, bet.event_id, bet.first_contender_win, bet.bet_win)
        return bet

    async def get_all(self, *, event_id: str | None = None) -> list[Bet]:
        """Возвращает все ставки

        фильтрует по `event_id`
        """
        data = await self._exec_sql("""
            SELECT * FROM bets WHERE event_id = $1;
        """, event_id)
        return [
            Bet(
                bet_id=str(raw_data['bet_id']),
                event_id=str(raw_data['event_id']),
                first_contender_win=raw_data['first_contender_win'],
                bet_win=raw_data['bet_win'],
            )
            for raw_data in data
        ]

    async def save(self, bet: Bet):
        """Изменяет ставку в БД"""
        await self._exec_sql("""
            UPDATE bets
            SET
                bet_win = $2
            WHERE bet_id = $1;
        """, bet.bet_id, bet.bet_win)

    async def delete_all(self):
        """Удаляет все записи о ставках"""
        self._data = []

    async def _exec_sql(self, sql, *sql_params):
        async with self._pool.acquire() as connection:
            async with connection.transaction():
                data = await connection.fetch(sql, *sql_params)
        return data
