import enum
import uuid
from dataclasses import dataclass
from pydantic import BaseModel


class LocalBaseModel(BaseModel):
    pass


def generate_id() -> str:
    return str(uuid.uuid4())


class EventStatus(enum.Enum):
    WIN = 'WIN'
    LOSE = 'LOSE'
    IN_PROGRESS = 'IN_PROGRESS'


class Event(LocalBaseModel):
    """Событие с двумя правтивниками, побеждает один или второй, без счёта"""
    event_id: str
    # true - если победитель - первый противник
    # false - если победитель - второй противник
    status: EventStatus


class Bet(LocalBaseModel):
    bet_id: str
    event_id: str
    # предполагаемый исход
    # true - ставка предполагает выйгрыш первого игрока
    # false - выйгрыш второго игрока
    first_contender_win: bool

    # true - если ставка выйграла
    # false - если ставка проиграла
    # none - если ставка ещё не сыграла
    bet_win: bool | None = None
