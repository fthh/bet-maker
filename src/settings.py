import os
from dataclasses import dataclass


@dataclass
class Settings:
    WEB_SERVER_PORT = int(os.getenv('WEB_SERVER_PORT', '8087'))
    DB_DSN = os.getenv('DB_DSN')


environment = Settings()
