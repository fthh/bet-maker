# ---- Base python ----
FROM python:3.11 AS base
RUN apt-get update
WORKDIR /app


# ---- Dependencies ----
FROM base AS dependencies
RUN pip3 install poetry==1.7.1
RUN poetry config virtualenvs.create false
COPY ./poetry.lock .
COPY ./pyproject.toml .
RUN poetry install


# ---- Release ----
FROM dependencies AS build
WORKDIR /app
COPY src/ /app/
COPY . /app/
RUN chmod +x /app/entrypoint.sh
ENTRYPOINT ["/app/entrypoint.sh"]
