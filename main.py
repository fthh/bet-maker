import asyncio

from fastapi import FastAPI
from uvicorn import Config, Server

from src.service import create_app
from src.settings import environment

app = FastAPI()


async def main() -> None:
    global app
    app = await create_app(app)
    config = Config(
        app=app,
        host='0.0.0.0',
        port=environment.WEB_SERVER_PORT,
    )
    server = Server(config)
    await server.serve()

if __name__ == '__main__':
    asyncio.run(main())
